import os

SQLALCHEMY_DATABASE_URI = os.getenv(
    'SQLALCHEMY_DATABASE_URI', 'sqlite:////tmp/your_default.db')
SQLALCHEMY_TRACK_MODIFICATIONS = False

FLASK_DEBUG = os.getenv('FLASK_DEBUG', False)

HANGMAN_WORDS_CHOICES = [
    '3dhubs',
    'marvin',
    'print',
    'filament',
    'order',
    'layer'
]

HANGMAN_WRONG_GUESSES = 5

HANGMAN_HIGHSCORE_FACTOR = 20
