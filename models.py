from flask import current_app as app
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import UUID
from uuid import uuid4

from sqlalchemy.ext.hybrid import hybrid_property
db = SQLAlchemy()


class Game(db.Model):
    __tablename__ = 'game'

    id = db.Column(UUID(as_uuid=True), primary_key=True, default=uuid4)
    gamer = db.Column(db.String(100), nullable=False)
    correct_word = db.Column(db.String(100), nullable=False)
    guesses = db.Column(db.String(100))

    def __repr__(self):
        return '<Game %r>' % self.id

    @hybrid_property
    def status(self):
        if self.score is not None:
            if self.score:
                return 'won'
            else:
                return 'game over'
        elif self.guesses:
            if self.guesses[-1] in self.correct_word:
                return 'correct attempt'
            else:
                return 'incorrect attempt'
        else:
            return 'new'

    @hybrid_property
    def wrong_guesses_left(self):
        wrong_guesses_left = app.config['HANGMAN_WRONG_GUESSES']

        if self.guesses:
            wrong_guesses_left -= len(
                [c for c in self.guesses if c not in self.correct_word])

        return wrong_guesses_left

    @hybrid_property
    def masked_word(self):
        if not self.guesses:
            return ' '.join(['_' for _ in self.correct_word])

        return ' '.join(
            [(c if c in self.guesses else '_') for c in self.correct_word])

    @hybrid_property
    def score(self):
        if self.wrong_guesses_left:
            if self.guesses:
                for c in self.correct_word:
                    if c not in self.guesses:
                        return None
                return (
                    self.wrong_guesses_left
                    * app.config['HANGMAN_HIGHSCORE_FACTOR'])
            return None

        else:
            return 0
