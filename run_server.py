from flask import Flask, redirect
from flask_migrate import Migrate

from api import blueprint as api_blueprint
from models import db


def initialize_app():
    app = Flask(__name__)
    app.config.from_pyfile('config.py')
    Migrate(app, db)
    db.init_app(app)
    app.register_blueprint(api_blueprint, url_prefix='/api/v1')
    return app


if __name__ == "__main__":
    app = initialize_app()
    app.run(debug=app.config['FLASK_DEBUG'])

    @app.route('/')
    def index():
        return redirect('/api/v1/docs', code=302)
