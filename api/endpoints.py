from flask_restplus import Resource

from api import api
from api.business import start_new_game, attempt_new_guess
from api.serializers import game, guess


@api.route('/ping')
class PingPong(Resource):
    def get(self):
        return {'message': 'pong'}


@api.route('/game')
class GameList(Resource):
    @api.response(201, 'Started new game')
    @api.expect(game, validate=True)
    @api.marshal_with(game)
    def post(self):
        """
        Starts a new Hangman game.
        """
        return start_new_game(api.payload), 201


@api.route('/game/<string:uid>/guess')
class GameDetail(Resource):
    @api.response(200, 'Game in progress')
    @api.response(403, 'No more guesses allowed')
    @api.expect(guess, validate=True)
    @api.marshal_with(game)
    def post(self, uid):
        """
        Attempts a new guess.
        """
        return attempt_new_guess(uid, api.payload)
