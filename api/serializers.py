from flask_restplus import fields

from api import api


game = api.model(
    'Game',
    {
        'id': fields.String(
            readonly=True,
            description="Publicly exposed non-guessable UUID of a game"),
        'gamer': fields.String(
            required=True,
            description="Gamer's name",
            min_length=1),
        'status': fields.String(readonly=True, description="Game's status"),
        'masked_word': fields.String(
            readonly=True,
            description="Masked word to show to player"),
        'wrong_guesses_left': fields.Integer(
            readonly=True,
            description="Number of incorrect guesses user can still make"),
        'score': fields.Integer(
            readonly=True, description="Player's score, if game is finished")
    })

guess = api.model(
    'Guess',
    {
        'character': fields.String(
            required=True,
            description="Next guess: 1 character, letter or number",
            min_length=1,
            max_length=1),
    })
