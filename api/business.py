from flask import current_app as app
from random import choice

from models import db, Game


def start_new_game(data):
    gamer = data.get('gamer')
    game = Game(
        gamer=gamer, correct_word=choice(app.config['HANGMAN_WORDS_CHOICES']))
    db.session.add(game)
    db.session.commit()
    return game


def attempt_new_guess(uid, data):
    game = Game.query.get(uid)
    if not game.wrong_guesses_left:
        return game, 403
    character = data.get('character')
    if game.guesses is None:
        game.guesses = ''
    game.guesses += character.lower()
    db.session.add(game)
    db.session.commit()
    return game, 200
