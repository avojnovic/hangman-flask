# Hangman Flask

Hangman game = Flask REST API + Python program as client

## Requirements

To run this project you will need:
- Python 3
- pipenv
- PostgreSQL

## Setup

### Database setup

Using `psql`, create new database for the project and user you'll use to connect to it:
```
CREATE USER hangman WITH PASSWORD 'hangman' CREATEDB;
CREATE DATABASE hangman with OWNER hangman ENCODING 'unicode';
```

### Python environment setup

1. Create new Python environment:

    `pipenv shell`

2. Install required packages:

    `pipenv install`

3. Copy `env.sample` to `.env`:

    `cp env.sample .env`

4. Modify `.env` configuration if needed. Note that, if you have used the same user/pass/db name for database setup, you don't need to change `SQLALCHEMY_DATABASE_URI` .


## Database migrations
To generate new migrations if schema changes, run `make dbmigrate`.

To apply any unapplied migrations, run `make dbupgrade` .

## Run server

To install any new packages and apply new migrations, run `make upgrade`.
After that, run `make server`. Site should be available on http://127.0.0.1:5000/ .
Visiting that root url will redirect you to Swagger docs endpoint.

## Run client

First, make sure you are in right Python environment - you can use the same as the one for server, but any Python 3.4+ with `requests` installed should work.
You simply run `python run_client.py` and play. If you have server on address other than http://127.0.0.1:5000/, you can tell client to use it via -u or --url option:
`python run_client.py -u https://my-other-host:5555/`

## Linter
Code is linted using flake8 linter tool. To lint the code, run `make lint`.


## Tests
Code is tested using pytest. To run tests, run `make test`. Beware, as tests are using `SQLALCHEMY_DATABASE_URI` db.