server:
	pipenv run python run_server.py;

dbmigrate:
	pipenv run flask db migrate;

dbupgrade:
	pipenv run flask db upgrade;

upgrade: dbupgrade
	pipenv install;

test:
	pipenv run python -m pytest

lint:
	pipenv run flake8
