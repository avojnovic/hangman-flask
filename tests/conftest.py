import pytest
from run_server import initialize_app


@pytest.fixture
def app():
    return initialize_app()
