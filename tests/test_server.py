import json

from models import Game


def _post_on_game(client, data):
    return client.post(
        'api/v1/game', data=json.dumps(data), content_type='application/json')


def _post_on_guess(client, uid, character):
    return client.post(
        'api/v1/game/%s/guess' % uid,
        data=json.dumps({'character': character}),
        content_type='application/json')


def test_start_new_game(client):
    res = _post_on_game(client, {'gamer': 'Gabriel'})

    assert res.status_code == 201
    assert 'id' in res.json
    assert res.json.get('gamer') == 'Gabriel'
    assert res.json.get('status') == 'new'
    assert '_' in res.json.get('masked_word')
    assert res.json.get('wrong_guesses_left') == 5
    assert res.json.get('score') is None
    assert 'correct_word' not in res.json
    game = Game.query.get(res.json['id'])
    assert game.gamer == 'Gabriel'
    assert len(game.correct_word) > 0


def test_start_new_game_without_gamer(client):
    game_count = Game.query.count()
    res = _post_on_game(client, {'gamer': ''})

    assert res.status_code == 400
    assert 'payload validation failed' in res.json.get('message')
    assert 'gamer' in res.json.get('errors')
    assert Game.query.count() == game_count


def test_correct_attempt(client):
    res = _post_on_game(client, {'gamer': 'Gabriel'})
    game = Game.query.get(res.json['id'])

    res = _post_on_guess(client, game.id, game.correct_word[0])
    assert res.status_code == 200
    assert res.json.get('gamer') == 'Gabriel'
    assert res.json.get('status') == 'correct attempt'
    assert res.json.get('wrong_guesses_left') == 5
    assert res.json.get('score') is None


def test_correct_attempt_upper(client):
    res = _post_on_game(client, {'gamer': 'Gabriel'})
    game = Game.query.get(res.json['id'])

    res = _post_on_guess(client, game.id, game.correct_word[0].upper())
    assert res.status_code == 200
    assert res.json.get('gamer') == 'Gabriel'
    assert res.json.get('status') == 'correct attempt'
    assert res.json.get('wrong_guesses_left') == 5
    assert res.json.get('score') is None


def test_incorrect_attempt(client):
    res = _post_on_game(client, {'gamer': 'Gabriel'})
    game = Game.query.get(res.json['id'])

    res = _post_on_guess(client, game.id, '0')
    assert res.status_code == 200
    assert res.json.get('gamer') == 'Gabriel'
    assert res.json.get('status') == 'incorrect attempt'
    assert res.json.get('wrong_guesses_left') == 4
    assert res.json.get('score') is None


def test_win(client):
    res = _post_on_game(client, {'gamer': 'Gabriel'})
    game = Game.query.get(res.json['id'])
    won = False
    character_index = 0

    while not won:
        res = _post_on_guess(
            client, game.id, game.correct_word[character_index])
        assert res.status_code == 200
        assert res.json.get('gamer') == 'Gabriel'
        assert res.json.get('status') in ['correct attempt', 'won']
        won = (res.json.get('status') == 'won')
        assert res.json.get('wrong_guesses_left') == 5
        if won:
            assert res.json.get('score') == 100
        else:
            assert res.json.get('score') is None
        character_index += 1


def test_fail(client):
    res = _post_on_game(client, {'gamer': 'Gabriel'})
    game = Game.query.get(res.json['id'])
    game_over = False

    while not game_over:
        res = _post_on_guess(client, game.id, '0')
        assert res.status_code == 200
        assert res.json.get('gamer') == 'Gabriel'
        assert res.json.get('status') in ['incorrect attempt', 'game over']
        game_over = (res.json.get('status') == 'game over')
        assert res.json.get('wrong_guesses_left') < 5
        if game_over:
            assert res.json.get('score') == 0
            assert res.json.get('wrong_guesses_left') == 0
        else:
            assert res.json.get('score') is None

    res = _post_on_guess(client, game.id, '0')
    assert res.status_code == 403
