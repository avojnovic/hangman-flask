import getopt
import requests
import sys
from urllib.parse import urljoin


DEFAULT_ROOT_URL = 'http://localhost:5000/'


def play_hangman(api_url, gamer):
    print('Nice to "meet" you, %s. Let\'s play Hangman!' % gamer)

    try:
        res = requests.post(urljoin(api_url, 'game'), json={'gamer': gamer})
        res.raise_for_status()
        game_id = res.json()['id']
        print("Hurrah! We're playing!")

        finished = False
        while not finished:
            print()
            print(res.json()['masked_word'])
            print()
            new_guess = input("What is your next guess? ").strip()
            if len(new_guess) != 1:
                print('Only and at least one character at time, please!')
                continue

            res = requests.post(
                urljoin(api_url, 'game/') + game_id + '/guess',
                json={'character': new_guess})
            res.raise_for_status()
            if res.json()['status'] == 'incorrect attempt':
                print("""
                    Bad luck! But, you have %s more incorrect guesses to spend.
                    """ % res.json()['wrong_guesses_left'])
            elif res.json()['status'] == 'correct attempt':
                print("""
                    Correct!
                    """)
            elif res.json()['status'] == 'game over':
                print("""
                    Game over, %s!
                    """ % gamer)
                finished = True
            elif res.json()['status'] == 'won':
                print("""
                    %s
                """ % res.json()['masked_word'].upper())
                print("""
                    Congratulations, %s! You won with score %s!
                    """ % (gamer, res.json()['score']))
                finished = True
            else:
                raise Exception(
                    '"%s" is not a game status I know of' % (
                        res.json()['status']))

    except Exception as e:
        print("""
            Sorry %s, something went wrong. More info below, and if you can
            figure out how to fix it, maybe we can still play!""" % gamer)
        print(e)


def main(argv):
    root_url = DEFAULT_ROOT_URL

    try:
        opts, args = getopt.getopt(argv, 'u:', ['url='])
    except getopt.GetoptError:
        print('run_client.py -u <url>')
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-u", "--url"):
            root_url = arg

    api_url = urljoin(root_url, '/api/v1/')
    res = requests.get(api_url + 'ping')
    if res.ok and res.json()['message'] == 'pong':
        name = input("Hi! What's your name? ").strip()

        if name:
            play_hangman(api_url, name)
        else:
            print("If you don't want to tell me your name, we can't play!")
    else:
        print("%s is not available - doesn't respond to ping" % api_url)
        try:
            res.raise_for_status()
        except Exception as e:
            print(e)


if __name__ == "__main__":
    main(sys.argv[1:])
